<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PartnersController extends Controller
{
    private function getPartners()
    {
        // Load partners from the file
        return collect(require base_path('data/partners.php'));
    }

    private function savePartners($partners)
    {
        // Save the updated partners back to the file
        file_put_contents(
            base_path('data/partners.php'),
            '<?php return ' . var_export($partners->all(), true) . ';'
        );
    }

    // Get all partners (Read)
    public function index()
    {
        return response()->json($this->getPartners());
    }

    // Get a single partner by UUID (Read)
    public function show($id)
    {
        $partner = $this->getPartners()->firstWhere('id', $id);

        if (!$partner) {
            return response()->json(['error' => 'Partner not found'], 404);
        }

        return response()->json($partner);
    }

    // Create a new partner (Create)
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'image' => 'required',
            'phone' => 'required',
        ]);

        // Get all partners (this is your custom logic)
        $partners = $this->getPartners();

        // Manually check if the email already exists in the partners data
        $existingPartner = $partners->firstWhere('email', $validated['email']);

        if ($existingPartner) {
            return response()->json(['message' => 'Email already exists'], 400); // Conflict (400 series)
        }

        // Generate a new UUID for the new partner
        $newUuid = Str::uuid()->toString();

        // Add the new partner
        $newPartner = array_merge($validated, ['id' => $newUuid]);

        // Add the new partner to the list
        $partners->push($newPartner);
        $this->savePartners($partners);

        return response()->json($partners->values()->all(), 201);
    }

    // Update an existing partner (Update)
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'image' => 'required',
            'phone' => 'required',
        ]);

        $partners = $this->getPartners();
        $partnerIndex = $partners->search(fn($partner) => $partner['id'] == $id);

        if ($partnerIndex === false) {
            return response()->json(['error' => 'Partner not found'], 404);
        }

        $partner = $partners->get($partnerIndex);
        $updatedPartner = array_merge($partner, $validated);

        $partners->put($partnerIndex, $updatedPartner);
        $this->savePartners($partners);

        return response()->json($updatedPartner);
    }

    // Delete a partner (Delete)
    public function destroy($id)
    {
        $partners = $this->getPartners();
        $partnerIndex = $partners->search(fn($partner) => $partner['id'] == $id);

        if ($partnerIndex === false) {
            return response()->json(['error' => 'Partner not found'], 404);
        }

        $partners->forget($partnerIndex);
        $partners = $partners->values(); // Reindex the collection

        $this->savePartners($partners);

        return response()->json($partners->all()); // Return as array
    }
}
