<?php return array (
  0 =>
  array (
    'id' => '0f04913c-9f3e-4093-9894-94295982d025',
    'name' => 'Reducations',
    'image' => 'https://www.reducations.com/templates/img/r-logo.png',
    'email' => 'reducations@steets.nl',
    'phone' => '+31 2321 1233',
  ),
  1 =>
  array (
    'id' => '45e05d3f-f2be-41c0-9b4d-a50265981c31',
    'name' => 'CLV',
    'image' => 'https://www.clv.nl/templates/img/logo.svg',
    'email' => 'clv@steets.nl',
    'phone' => '+31 2321 1233',
  ),
  2 =>
  array (
    'id' => 'fa0a5d00-55f1-4552-b043-57fe1df85dda',
    'name' => 'Bruil 123 123',
    'image' => 'https://www.bruil.nl/templates/img/bruil-logo.png',
    'email' => 'bruil@steets.nl',
    'phone' => '+31 2321 1233',
  ),
  3 =>
  array (
    'id' => '64b0176c-fcd0-42ce-ae62-c2732aa20d7d',
    'name' => 'Yellow Spring',
    'image' => 'https://www.yellowspring.nl/templates/img/logo-black.svg',
    'email' => 'yellow_spring@steets.nl',
    'phone' => '+31 2321 1233',
  ),
  4 =>
  array (
    'id' => '9fd53fba-6bd3-41e7-b9ec-ae56c309a1b7',
    'name' => 'Werken bij Alfa',
    'image' => 'https://www.werkenbijalfa.nl/templates/img/alfa-logo.svg',
    'email' => 'alfa@steets.nl',
    'phone' => '+31 2321 1233',
  ),
  5 =>
  array (
    'id' => '4aa3526d-631d-4e95-9dcf-7917bfce8ed5',
    'name' => 'Toyota Van Gent',
    'image' => 'https://www.vangent.nl/templates/img/Logo-van-Gent-RGB.png',
    'email' => 'van_gent@steets.nl',
    'phone' => '+31 2321 1233',
  ),
  6 =>
  array (
    'id' => '7f0e1ead-0329-44f0-914f-1e3dd730e2e9',
    'name' => 'Bio Academy',
    'image' => 'https://bio-academy.acc9.steets.nl/templates/img/logo.svg',
    'email' => 'bio_academy@steets.nl',
    'phone' => '+31 2321 1233',
  ),
  7 =>
  array (
    'id' => '92516948-be34-47cd-a465-69c0da77b407',
    'name' => 'Vios Trappen',
    'image' => 'https://www.vios.nl/templates/img/vios-logo.svg',
    'email' => 'vios@steets.nl',
    'phone' => '+31 2321 1233',
  ),
  8 =>
  array (
    'id' => '2c90efcd-de3c-4cba-8d1e-cb9bbd9e4e14',
    'name' => 'Ellesie',
    'image' => 'https://www.ellesie.nl/templates/img/logo.svg',
    'email' => 'ellesie@steets.nl',
    'phone' => '+31 2321 1233',
  ),
  9 =>
  array (
    'id' => '2b90efcd-de3c-4cba-8d1e-cb9bbd9e4e14',
    'name' => 'Trition',
    'image' => 'https://www.trition.nl/templates/img/logo.png',
    'email' => 'trition@steets.nl',
    'phone' => '+31 2321 1233',
  ),
  10 =>
  array (
    'id' => '2d90efcd-de3c-4cba-8d1e-cb9bbd9e4e14',
    'name' => 'Zien en Geloven',
    'image' => 'https://www.zien-en-geloven.nl/templates/img/logo.svg',
    'email' => 'zien_geloven@steets.nl',
    'phone' => '+31 2321 1233',
  ),
);