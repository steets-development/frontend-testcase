<?php

use App\Http\Controllers\PartnersController;
use Illuminate\Support\Facades\Route;

Route::get('partners', [PartnersController::class, 'index']); // Read (List all)
Route::get('partners/{id}', [PartnersController::class, 'show']); // Read (Single item)
Route::post('partners', [PartnersController::class, 'store']); // Create
Route::put('partners/{id}', [PartnersController::class, 'update']); // Update
Route::delete('partners/{id}', [PartnersController::class, 'destroy']); // Delete