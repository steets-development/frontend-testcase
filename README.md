# Steets Frontend Development Testcase

We'd like to see how you handle this testcase. It's a fully functional module of a website - partners module. You can view / create / edit / add the partners in the frontend.

**Push your code on a separate branch from master, named by your name & lastname.**

When you finish, please create a pull request on master.

**Important note** on using git with bitbucket. Log in as a bitbucket user and clone the repository using **SSH** and then push to your branch. In order to set up **SSH** to your machine, please follow these steps: https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/

If you try and clone your repository using **HTTPS** you will not be able to push things.

<br>

## **Backend**

The backend is provided and you don't have to change anything there, you just have to run it. Steps to run:

1. Make sure you have PHP (>=8.1) & composer installed.
2. Run `composer install`
3. Run `php artisan serve`
4. The routes which you can use are as follows:

| HTTP Method | URL                     | Purpose             |
|-------------|-------------------------|---------------------|
| GET         | `api/partners`          | Read (List all)     |
| GET         | `api/partners/{id}`     | Read (Single item)  |
| POST        | `api/partners`          | Create              |
| PUT         | `api/partners/{id}`     | Update              |
| DELETE      | `api/partners/{id}`     | Delete              |

<br>

## **Frontend**

You can build the frontend using any framework you prefer—or even without a framework. For flexibility, it’s recommended to make the frontend a standalone project if you choose to use a framework.

The frontend should include the following:

1. **Homepage**
   1. Display a list of all partners.
   2. Include an “Add Partner” form.
      1. When a partner is added via this form, it should appear immediately in the partners list on the homepage.
      2. Additionally, the new partner should be saved to the backend using an AJAX call.
2. **Partner Detail Page**
   1. Each partner on the homepage should be a clickable link, navigating to a detail page for that partner.
   2. The detail page should have two buttons:
      1. **Edit Button**
         1. Toggles the “Edit Partner” form, prefilled with the partner’s current data.
         2. Any changes made in the form should update the partner on the backend via an AJAX call upon submission.
         3. Submitting the form should redirect back to the homepage.
      2. **Delete Button**
         1. Toggles a “Delete Modal.”
         2. Clicking the “Delete” button in the modal should remove the partner from the backend using an AJAX call.
         3. After deletion, redirect to the homepage.

A couple of notes for the development:

- Use whichever css framework/library you want for the styles. **Using Tailwindcss would be considered a plus.**
- Use a responsive **mobile first** approach.
- Set-up the header as a fixed bar that sticks to the top as you scroll downwards.
- Design (in the frontend) the menu when is open like shown in the Adobe.
- Use the materials provided alongside the Adobe.
- Design link if you can't open Adobe XD: https://xd.adobe.com/view/f9c06c59-595b-497d-9b7e-1b02c794edad-ac93/specs/